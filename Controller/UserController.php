<?php

namespace SteamUserBundle\Controller;

use SteamUserBundle\Entity\User;
use SteamUserBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserController extends Controller
{
    /**
     * Get current user.
     *
     * @return JsonResponse
     */
    public function getAction()
    {
        $user = $this->getUser();

        if (!empty($user)) {
            return new JsonResponse(
                $this->get('jms_serializer')->toArray($user),
                JsonResponse::HTTP_OK
            );

        }

        return new JsonResponse(
            ['message' => 'User not found'],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * Update current user
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putAction(Request $request)
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            return new JsonResponse(
                ['message' => 'User is not found'],
                JsonResponse::HTTP_NOT_FOUND
            );
        }

        $userForm = $this->createForm(UserType::class, $user);

        $userForm->handleRequest($request);

        if ($userForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(
                ['message' => 'User has updated successful'],
                JsonResponse::HTTP_ACCEPTED
            );
        } else {
            return new JsonResponse(
                ['message' => (string)$userForm->getErrors(true, false)],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Get login data
     *
     * @return JsonResponse
     */
    public function loginAction()
    {
        return new JsonResponse(
            [
                'action' => $this->getParameter('steam_url'),
                'data' => [
                    'openid.ns' => $this->getParameter('steam_openid_ns'),
                    'openid.mode' => $this->getParameter('steam_openid_mode'),
                    'openid.identity' => $this->getParameter('steam_openid_identity'),
                    'openid.claimed_id' => $this->getParameter('steam_openid_claimed_id'),
                    'openid.return_to' => $this->generateUrl('login_check', [], UrlGeneratorInterface::ABSOLUTE_URL),
                    'openid.realm' => $this->generateUrl('login_check', [], UrlGeneratorInterface::ABSOLUTE_URL),
                ],
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Logout user from site
     *
     * @return RedirectResponse
     */
    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);

        $session = $this->get('session');
        $session->invalidate();

        $response = $this->redirectToRoute('account_user_login_data');

        $cookieNames = [
            $this->getParameter('session.name'),
        ];

        foreach ($cookieNames as $cookieName) {
            $response->headers->clearCookie($cookieName);
        }

        return $response;
    }

    /**
     * Help-service controller, used by EventListener
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function afterLoginAction(Request $request)
    {
        $info = parse_url($request->getUri());
        $host = $info['host'];
        $host_names = explode(".", $host);
        $bottom_host_name = '';
        $i = (count($host_names) > 2) ? 1 : 0;

        for (; $i < count($host_names); $i++) {
            $bottom_host_name .= $host_names[$i].'.';
        }
        $bottom_host_name = rtrim($bottom_host_name, '.');

        return new RedirectResponse($info['scheme'].'://'.$bottom_host_name);
    }

    /**
     * Check trade_url and update partner and token from trade_url
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function checkTradeUrlAction(Request $request)
    {
        $tradeUrlService = $this->get('steam.trade.url.service');
        $status = $tradeUrlService->validateTradeUrl(trim($request->request->get('trade_url')));

        if ($status) {
            $tradeUrlService->updateTradeUrlParams();

            return new JsonResponse(
                ['message' => 'Trade url has updated successful'],
                JsonResponse::HTTP_ACCEPTED
            );
        }

        return new JsonResponse(
            ['message' => 'Trade url is not valid'],
            JsonResponse::HTTP_NOT_FOUND
        );
    }
}
