# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This bundle is for create account functionality in project

### How do I get set up? ###

* To set up first you need to add
"drakeroll/steam-user-bundle" into your composer.json with the last version of bundle. Then add to `Kernel.php`
```php 
        $bundles = [
        ...
        new SteamUserBundle\SteamUserBundle(),
        ...
        ];
```
         
* Add to `parameters.yml`:
```yml 
steam_user.check_avatar_route: account_user_redirect
```
And in the `config.yml` :
```yml
steam_user:
    session: '%session.name%'
    check_avatar_route: '%steam_user.check_avatar_route%'
```
        
* This bundle has dependency on SteamAuthBundle and Doctrine ORM.
* To add tests modify your `phpunit.xml`
```xml
<testsuite name="SteamUserBundle">
     <directory>vendor/drakeroll/steam-user-bundle/Tests</directory>
</testsuite>
```
