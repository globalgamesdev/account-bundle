<?php

namespace SteamUserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use SteamAuthBundle\Security\User\SteamUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 *
 * @ORM\MappedSuperclass()
 */
class User implements SteamUserInterface, UserInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;
    const STATUS_SANDBOX = 3;
    const STATUS_SYSTEM = 4;

    /**
     * User status on site
     *
     * @var array
     */
    public static $userStatus = [
        self::STATUS_ACTIVE => "Active",
        self::STATUS_DISABLED => "Disabled",
        self::STATUS_SANDBOX => "Sandbox",
        self::STATUS_SYSTEM => "System"
    ];

    /**
     * User ID
     *
     * @var integer
     *
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * User avatar
     *
     * @var string
     *
     * @ORM\Column(name="avatar",type="string",length=191)
     */
    protected $avatar;

    /**
     * User full name
     *
     * @var string
     *
     * @ORM\Column(name="username",type="string",length=191)
     * @Assert\NotBlank
     */
    protected $username;

    /**
     * @ORM\Column(name="password",type="string",length=191)
     * @Assert\NotBlank
     */
    protected $password;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $roles = [];

    /**
     * User Steam nickname
     *
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=191)
     * @Assert\NotBlank
     */
    protected $nickname;

    /**
     * User email
     *
     * @var string
     *
     * @ORM\Column(name="email",type="string",length=191, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     */
    protected $email;

    /**
     * User Steam partner
     * @var integer
     *
     * @ORM\Column(name="partner", type="integer", length=8, nullable=true)
     */
    protected $partner;

    /**
     * User Steam token
     *
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=191, nullable=true)
     */
    protected $token;

    /**
     * User status
     *
     * @var integer
     *
     * @ORM\Column(name="status",type="integer",length=2)
     * @Assert\Choice(callback = "getStatuses")
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * User UTM
     * @var string
     *
     * @ORM\Column(name="utm", type="string", length=191, nullable=true)
     */
    protected $utm;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * Get Utm
     *
     * @return float
     */
    public function getUtm()
    {
        return $this->utm;
    }

    /**
     * Set Utm
     *
     * @param $utm
     *
     * @return $this
     */
    public function setUtm($utm)
    {
        $this->utm = $utm;

        return $this;
    }


    /**
     * Return name of status by code
     *
     * @return mixed|null
     */
    public function getStatus()
    {
        if (!is_null($this->status)) {
            return self::$userStatus[$this->status];
        } else {
            return null;
        }
    }

    /**
     * Return array of all status
     *
     * @return array
     */
    public static function getStatusList()
    {
        return self::$userStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Validate status. Must be from array self::$userStatus
     *
     * @return array
     */
    public static function getStatuses()
    {
        return array_keys(self::$userStatus);
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        if (!is_null($email)) {
            $this->email = $email;
        }

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get Avatar
     *
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return $this
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * implements UserInterface
     *
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * For implements UserInterface
     */
    public function eraseCredentials()
    {

    }

    /**
     * Return user role
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;

        return $roles;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set default roles on prePersist event
     *
     * @ORM\PrePersist
     */
    public function preCreateRole()
    {
        $roles = $this->roles->toArray();

        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
        $this->setRoles($roles);

    }

    /**
     * Get partner from trade link
     *
     * @return integer
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set partner from trade link
     *
     * @param $partner
     *
     * @return $this
     */
    public function setPartner($partner)
    {
        if (!is_null($partner)) {
            $this->partner = $partner;
        }

        return $this;
    }

    /**
     * Get token from trade link
     *
     * @return integer
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token from trade link
     *
     * @param $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        if (!is_null($token)) {
            $this->token = $token;
        }

        return $this;
    }
}
