<?php

namespace SteamUserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SteamUserExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('steam_user.default_avatar', $config['default_avatar']);
        $container->setParameter('steam_user.check_avatar_route', $config['check_avatar_route']);
        $container->setParameter('steam_user.after_check_avatar_route', $config['after_check_avatar_route']);
        $container->setParameter('steam_user.upload_dir', $config['upload_dir']);
        $container->setParameter('steam_user.upload_path', $config['upload_path']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
