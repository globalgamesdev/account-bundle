<?php

namespace SteamUserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder
            ->root('steam_user')
                ->children()
                    ->scalarNode('default_avatar')
                        ->info('Default avatar of user')
                        ->defaultValue('default.jpg')
                    ->end()
                    ->scalarNode('check_avatar_route')
                        ->defaultValue('account_user_redirect')
                    ->end()
                    ->scalarNode('after_check_avatar_route')
                        ->defaultValue('home')
                    ->end()
                    ->scalarNode('upload_dir')
                        ->defaultValue('uploads')
                    ->end()
                    ->scalarNode('upload_path')
                        ->defaultValue('%kernel.root_dir%/../web/uploads/')
                    ->end()
                    ->scalarNode('session')
                    ->end()
                ->end();

        return $treeBuilder;
    }
}
