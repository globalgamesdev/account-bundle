<?php

namespace SteamUserBundle\EventListener;


use SteamUserBundle\Service\AvatarServiceInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class RequestListener
 * @package AccountBundle\EventListener
 */
class RequestListener
{
    /**
     * @var AvatarServiceInterface
     */
    private $service;

    /**
     * @var string
     */
    private $route;

    /**
     * RequestListener constructor.
     *
     * @param $service
     * @param $route
     */
    public function __construct($service, $route)
    {
        $this->service = $service;
        $this->route = $route;
    }

    /**
     * When user login check his avatar
     *
     * @param GetResponseEvent $event
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->attributes->get('_route') == $this->route) {
            $this->service->updateAvatar();
        }
    }
}
