<?php
namespace SteamUserBundle\Service;

interface AvatarServiceInterface
{
    /**
     * Update avatar from steam
     *
     * @return bool
     */
    public function updateAvatar();

    /**
     * Get url by file name
     *
     * @param string $fileName
     *
     * @return string The current url of File
     */
    public function getAvatarPath($fileName);

    /**
     * Generate unique avatar name
     *
     * @param $userName
     *
     * @param $algorithm
     *
     * @return bool
     */
    public function generateAvatar($userName, $algorithm);
}
