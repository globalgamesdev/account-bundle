<?php

namespace SteamUserBundle\Service;

use SteamUserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use SteamAuthBundle\Service\SteamUserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class AvatarService
 * @package AccountBundle\Service
 */
class AvatarSaveFS implements AvatarServiceInterface, AvatarStorageInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SteamUserService
     */
    private $steam;
    /**
     * @var string
     */
    private $uploadPath;
    /**
     * @var string
     */
    private $uploadDir;
    /**
     * @var string
     */
    private $defAvatar;

    /**
     * @var Entity
     */
    private $user;

    /**
     * @var string
     */
    private $userAvatar;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var ContainerInterface;
     */
    private $container;
    /**
     * @var string
     */
    private $extension;

    /**
     * AvatarService constructor.
     *
     * @param EntityManager $entityManager
     * @param SteamUserService $steam
     * @param ContainerInterface $container
     * @param Filesystem $fs
     */
    public function __construct(
        EntityManager $entityManager,
        SteamUserService $steam,
        ContainerInterface $container,
        Filesystem $fs,
        $uploadPath,
        $uploadDir,
        $defAvatar
    ) {
        $this->entityManager = $entityManager;
        $this->steam = $steam;
        $this->fs = $fs;
        $this->container = $container;

        $this->uploadPath = $uploadPath;
        $this->uploadDir = $uploadDir;
        $this->defAvatar = $defAvatar;
    }

    /**
     * Update avatar by fullAvatar
     *
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAvatar()
    {
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!$this->user instanceof User) {
            return false;
        }

        //load user data from steam
        $this->userAvatar = $this->steam->getUserData($this->user->getUserName());
        $userName = $this->userAvatar['personaname'];

        if (null === $this->userAvatar['avatarfull']) {
            $this->userAvatar = $this->getAvatarPath($this->defAvatar);
        } else {
            $this->userAvatar = $this->userAvatar['avatarfull'];
        }

        //get extension
        $this->extension = pathinfo($this->userAvatar, PATHINFO_EXTENSION);
        //generate avatar name for user
        $fileName = $this->generateAvatar($this->user->getUserName());

        //save to fs
        $this->userAvatar = $this->saveAvatar(
            $this->userAvatar,
            $this->uploadPath.$fileName
        ) ? $fileName : $this->defAvatar;

        //save to db
        $this->user->setNickname($userName);
        $this->user->setAvatar($this->getAvatarPath($this->userAvatar));
        $this->entityManager->persist($this->user);
        $this->entityManager->flush($this->user);

        return true;
    }

    /**
     * Generate hash name for user avatar with file extension
     *
     * @param $userName
     *
     * @param string $algorithm
     *
     * @return string '
     */
    public function generateAvatar($userName, $algorithm = '')
    {
        return $userName.'.'.$this->extension;
    }

    /**
     * Return return file url
     *
     * @param string $fileName
     *
     * @return string
     */
    public function getAvatarPath($fileName)
    {
        return "http".($_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['SERVER_NAME'].'/'.$this->uploadDir.'/'.$fileName;
    }

    /**
     * Save avatar from steam url to fs system
     *
     * @param $from
     * @param $to
     *
     * @return bool
     */
    public function saveAvatar($from, $to)
    {
        try {
            $this->fs->copy($from, $to, true);
        } catch (FileNotFoundException $error) {
            return false;
        } catch (IOException $error) {
            return false;
        }

        return true;
    }

    /**
     * Delete file from fs system
     *
     * @param $filePath
     *
     * @return bool
     */
    public function deleteAvatar($filePath)
    {
        if ($this->fs->exists($filePath)) {
            try {
                $this->fs->remove($filePath);
            } catch (IOException $error) {
                return false;
            }
        }

        return true;
    }
}
