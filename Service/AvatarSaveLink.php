<?php

namespace SteamUserBundle\Service;

use SteamUserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use SteamAuthBundle\Service\SteamUserService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AvatarSaveLink
 * @package AccountBundle\Service
 */
class AvatarSaveLink implements AvatarServiceInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SteamUserService
     */
    private $steam;

    /**
     * @var Entity
     */
    private $user;

    /**
     * @var string
     */
    private $userAvatar;

    /**
     * @var ContainerInterface;
     */
    private $container;

    /**
     * AvatarService constructor.
     *
     * @param EntityManager $entityManager
     * @param SteamUserService $steam
     * @param ContainerInterface $container
     */
    public function __construct(
        EntityManager $entityManager,
        SteamUserService $steam,
        ContainerInterface $container
    ) {
        $this->entityManager = $entityManager;
        $this->steam = $steam;
        $this->container = $container;
    }

    /**
     * Update user avatar
     *
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateAvatar()
    {
        $this->user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!$this->user instanceof User) {
            return false;
        }

        $this->userAvatar = $this->getAvatarPath($this->user->getUserName());

        if (null === $this->userAvatar['avatarfull']) {
            throw new \Exception('Not found Avatar on Steam');
        }

        //save to db
        $this->user->setNickname($this->userAvatar['personaname']);
        $this->user->setAvatar($this->userAvatar['avatarfull']);
        $this->entityManager->persist($this->user);
        $this->entityManager->flush($this->user);

        return true;
    }

    /**
     * Generate avatar path
     *
     * @param string $userName
     *
     * @return string
     */
    public function getAvatarPath($userName)
    {
        return $this->steam->getUserData($userName);
    }

    /**
     * Generate avatar name
     *
     * @param $avatar
     * @param $algorithm
     *
     * @return mixed
     */
    public function generateAvatar($avatar, $algorithm)
    {
        return null;
    }
}
