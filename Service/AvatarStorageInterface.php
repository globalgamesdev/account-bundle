<?php

namespace SteamUserBundle\Service;


interface AvatarStorageInterface
{
    /**
     * Save file by name to upload folder
     *
     * @param $from
     * @param $to
     *
     * @return bool
     */
    public function saveAvatar($from, $to);


    /**
     * Delete file by name
     *
     * @param $fileName
     *
     * @return bool|int|mixed
     */
    public function deleteAvatar($fileName);
}
