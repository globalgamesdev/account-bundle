<?php

namespace SteamUserBundle\Tests;

use SteamUserBundle\Entity\User;
use SteamUserBundle\Service\AvatarSaveLink;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\MessageInterface;
use SteamAuthBundle\Service\SteamUserService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class AvatarSaveLinkTest
 * @package AccountBundle\Tests
 */
class AvatarSaveLinkTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var SteamUserService
     */
    private $steam;

    /**
     * @var TokenStorage
     */
    private $token;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $responseJson;

    /**
     * @var MessageInterface
     */
    private $response;

    /**
     * @var TokenInterface
     */
    private $tokenInterface;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = $this->createMock(EntityManager::class);
        $this->token = $this->createMock(TokenStorage::class);
        $this->user = $this->createMock(User::class);
        $this->response = $this->createMock(MessageInterface::class);
        $this->tokenInterface = $this->createMock(TokenInterface::class);
        $this->steam = $this->createMock(SteamUserService::class);
        $this->container = $this->createMock(ContainerInterface::class);

        $this->responseJson = ['avatarfull' => 'text', 'personaname' => 'fake'];
    }

    /**
     * Test get avatar path
     */
    public function testGetAvatarPath()
    {
        $userName = 'fakeName';

        $this->steam->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue($userName));

        $avatar = new AvatarSaveLink(
            $this->em,
            $this->steam,
            $this->container
        );
        $this->assertEquals(
            $userName,
            $avatar->getAvatarPath($userName)
        );
    }

    /**
     * Test update Avatar
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testUpdateAvatar()
    {

        $this->user->expects($this->any())
            ->method('getUserName')
            ->will($this->returnValue('fakeName'));

        $this->user->expects($this->any())
            ->method('getAvatar')
            ->will($this->returnValue('fakeName.jpg'));

        $this->user->expects($this->any())
            ->method('setAvatar')
            ->will($this->returnValue(false));

        $this->tokenInterface->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue($this->user));

        $this->steam->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue($this->responseJson));

        $this->token->expects($this->once())
            ->method('getToken')
            ->will($this->returnValue($this->tokenInterface));

        $this->container->expects($this->any())
            ->method('get')
            ->will($this->returnValue($this->token));

        $avatar = new AvatarSaveLink(
            $this->em,
            $this->steam,
            $this->container
        );

        $this->assertTrue($avatar->updateAvatar());
    }

    /**
     * Test generate avatar
     */
    public function testGenerateAvatar()
    {
        $avatar = new AvatarSaveLink(
            $this->em,
            $this->steam,
            $this->container
        );

        $this->assertEquals(null, $avatar->generateAvatar('fake', ''));
    }
}
