<?php

namespace SteamUserBundle\Tests;

use SteamUserBundle\Entity\User;
use SteamUserBundle\Service\AvatarSaveFS;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\MessageInterface;
use SteamAuthBundle\Service\SteamUserService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class AvatarSaveFSTest
 * @package AccountBundle\Tests
 */
class AvatarSaveFSTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $uploadPath;
    /**
     * @var string
     */
    private $uploadDir;
    /**
     * @var string
     */
    private $defAvatar;

    /**
     * @var SteamUserService
     */
    private $steam;

    /**
     * @var TokenStorage
     */
    private $token;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $responseJson;

    /**
     * @var MessageInterface
     */
    private $response;

    /**
     * @var TokenInterface
     */
    private $tokenInterface;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->uploadDir = self::$kernel->getContainer()->getParameter('steam_user.upload_dir');
        $this->uploadPath = self::$kernel->getContainer()->getParameter('steam_user.upload_path');
        $this->defAvatar = self::$kernel->getContainer()->getParameter('steam_user.default_avatar');
        $this->responseJson = ['avatarfull' => 'text', 'personaname' => 'fake'];

        $this->em = $this->createMock(EntityManager::class);
        $this->token = $this->createMock(TokenStorage::class);
        $this->user = $this->createMock(User::class);
        $this->response = $this->createMock(MessageInterface::class);
        $this->tokenInterface = $this->createMock(TokenInterface::class);
        $this->fs = $this->createMock(Filesystem::class);
        $this->steam = $this->createMock(SteamUserService::class);
        $this->container = $this->createMock(ContainerInterface::class);


    }

    /**
     * Test get avatar path
     */
    public function testGetAvatarPath()
    {
        $_SERVER['HTTPS'] = 'http';
        $_SERVER['SERVER_NAME'] = 'drakeroll.com';
        $fileName = '.searchFile';

        $this->container->expects($this->any())
            ->method('getParameter')
            ->will($this->returnValue($this->uploadDir));

        $avatar = new AvatarSaveFS(
            $this->em,
            $this->steam,
            $this->container,
            $this->fs,
            $this->uploadPath,
            $this->uploadDir,
            $this->defAvatar
        );
        $this->assertEquals(
            "http".($_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['SERVER_NAME'].'/'.$this->uploadDir.'/'.$fileName,
            $avatar->getAvatarPath($fileName)
        );
    }

    /**
     * Test update Avatar
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testUpdateAvatar()
    {

        $this->user->expects($this->any())
            ->method('getUserName')
            ->will($this->returnValue('fakeNotIssetUserName'));

        $this->user->expects($this->any())
            ->method('getAvatar')
            ->will($this->returnValue('fakeNotIssetUserName.jpg'));

        $this->user->expects($this->any())
            ->method('setAvatar')
            ->will($this->returnValue(true));


        $this->user->expects($this->any())
            ->method('setNickname')
            ->will($this->returnValue(true));

        $this->steam->expects($this->any())
            ->method('getUserData')
            ->will($this->returnValue($this->responseJson));

        $this->tokenInterface->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue($this->user));

        $this->token->expects($this->once())
            ->method('getToken')
            ->will($this->returnValue($this->tokenInterface));

        $this->container->expects($this->any())
            ->method('get')
            ->will($this->returnValue($this->token));

        $this->fs->expects($this->once())
            ->method('copy')
            ->will($this->returnValue(true));

        $avatar = new AvatarSaveFS(
            $this->em,
            $this->steam,
            $this->container,
            $this->fs,
            $this->uploadPath,
            $this->uploadDir,
            $this->defAvatar
        );

        $this->assertTrue($avatar->updateAvatar());
    }

    /**
     * Test save file
     */
    public function testSaveAvatar()
    {
        $this->fs->expects($this->once())
            ->method('copy')
            ->will($this->returnValue(true));

        $avatar = new AvatarSaveFS(
            $this->em,
            $this->steam,
            $this->container,
            $this->fs,
            $this->uploadPath,
            $this->uploadDir,
            $this->defAvatar
        );
        $this->assertTrue($avatar->saveAvatar('fake', 'anotherFake'));
    }

    /**
     * Test for delete file
     */
    public function testDeleteAvatar()
    {
        $this->fs->expects($this->once())
            ->method('exists')
            ->will($this->returnValue(true));

        $this->fs->expects($this->once())
            ->method('remove')
            ->will($this->returnValue(true));

        $avatar = new AvatarSaveFS(
            $this->em,
            $this->steam,
            $this->container,
            $this->fs,
            $this->uploadPath,
            $this->uploadDir,
            $this->defAvatar
        );

        $this->assertTrue($avatar->deleteAvatar('fake'));
    }

    /**
     * Test generate avatar
     */
    public function testGenerateAvatar()
    {
        $avatar = new AvatarSaveFS(
            $this->em,
            $this->steam,
            $this->container,
            $this->fs,
            $this->uploadPath,
            $this->uploadDir,
            $this->defAvatar
        );

        $this->assertEquals('fake.', $avatar->generateAvatar('fake', ''));
    }
}
